﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public Rigidbody enemyBody;
    public Transform player;
    public GameEnding gameEnding;

    private Quaternion originalRotation;

    private float stunTime = 0;
    bool m_IsPlayerInRange;

    private void Start()
    {
        originalRotation = enemyBody.gameObject.transform.rotation;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    public void getStun(float seconds)
    {
        stunTime = seconds;
    }

    void FixedUpdate()
    {

        if (stunTime <= 0)
        {
            enemyBody.angularVelocity = Vector3.zero;
            stunTime -= Time.deltaTime;
            if (m_IsPlayerInRange)
            {
                Vector3 direction = player.position - transform.position + Vector3.up;
                Ray ray = new Ray(transform.position, direction);
                RaycastHit raycastHit;

                if (Physics.Raycast(ray, out raycastHit))
                {
                    if (raycastHit.collider.transform == player)
                    {
                        gameEnding.CaughtPlayer();
                    }
                }
            }
        } else
        {
            enemyBody.transform.Rotate(new Vector3(0f, 10f, 0f));
            stunTime -= Time.deltaTime;
            if(stunTime <= 0)
            {
                //enemyBody.gameObject.transform.rotation = originalRotation;
            }
        }

        //Debug.Log("Stunned for" + stunTime);
    }
}