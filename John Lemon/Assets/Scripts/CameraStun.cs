﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStun : MonoBehaviour
{
    public int stunTime;
    public GameObject opponent;

    private bool enemyInRange;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemyInRange = true;
            opponent = other.transform.parent.gameObject;
            //Debug.Log("Enemy In Range");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemyInRange = false;
            opponent = null;
            //Debug.Log("Enemy out of Range");

        }
    }

    void Update()
    {
        if (opponent!=null && enemyInRange && Input.GetKeyDown("space"))
        {
            //Debug.Log("Opp = "+opponent.gameObject.GetComponentInChildren<Observer>());
            opponent.gameObject.GetComponentInChildren<Observer>().getStun(3.58f);
        }
    }
}
